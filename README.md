# RoR JS boilerplate - by FourStacks #

This basic JS structure has been created in order to get JS authoring for RoR projects up and running quickly
and to improve consistency between projects.  Each file is commented and whilst much of the existing JS contained
within each file is optional, much of it is generic and could be used across most responsive web applications.  However,
there is no need to use everything included in this boilerplate and if certain tools such as Modernizr are not required then
you are encourage to fork this boilerplate and adapt it to suit your needs and workflow