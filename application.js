/* MAIN JS SCAFFOLD FILE */

// Call jQuery first

//= require jquery
//= require jquery_ujs


/*
Files in /libs are library files which generally add some functionality to the site without needing to be initialised,
for example modernizr, selectivizr or the html5 shiv.  Most of these can be compiled as part of the main JS file though others,
such as Modernizr, which are better placed in the head of the site, you may wish to call seperately.  If that is the case then 
consider putting a minified version of the file into libs as it will not otheriwse by minified by the asset pipeline 
(most of the time it's better to have the development version for easier debugging)
*/

//= require ./libs/html5shiv
//= require ./libs/respond
//= require ./libs/selectivizr


/*
Files in /vendor are plugin files which are authored by a 3rd party.  Examples might include carousel scripts or masonary 
(though there are countless others).  Most of these plugins will need to be initialised (see below). You should place the unminified
(development) versions of these files into modules and call them as in the example below:
*/

//= require ./vendor/example-plugin

/*
Files in /modules are self-authored, usually self-contained scripts that add bespoke functionality to parts of the site.  Examples might be a 
simple accordion script or a mobile navigation show/hide switch.  A handy generic module has been included that contains some helpers for 
responsive JS
*/

//= require ./modules/res-breakpoints



/*
There are two files /application.  The first is an inits file which contains any required initalisations of included JS plugins.  The 
second is a snippets file that contains any very short bit of JS functionality that are not really worth abstracting into their own module
*/

//= require ./application/snippets
//= require ./application/inits
